#include <iostream>


class Animal {
public:
	virtual void Voice()
	{
		std::cout << "Im Animal\n";
		
	};
	virtual ~Animal() { std::cout << "destroy Animal\n"; }
};

class Dog : public Animal {
public:
	 void Voice() override {
		std::cout << "Im Dog\n";
		
	};
	 ~Dog() { std::cout << "destroy Dog\n"; }
};

class Cat : public Animal {
public:
	 void Voice() override{
		std::cout << "Im Cat\n";
		
	};
	 ~Cat() { std::cout << "destroy Cat\n"; }
};

class Fox : public Animal{
public:
	 void Voice() override {
		std::cout << "Im Fox\n";
		
	};
	 ~Fox() { std::cout << "destroy Fox\n"; }
};


int main()
{
	
	
	
	

	Animal* ar[] = {new Dog,new Cat, new Fox};
	
	
	for (int i = 0;i < 3;i++) {
		ar[i]->Voice();
		
	};
	for (int i = 0;i < 3;i++) {
		delete[] ar[i];
		ar[i] = nullptr;
	}
	return 0;
}